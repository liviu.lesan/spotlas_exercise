package repository

import (
	"database/sql"
	"spotlas/internal/model"
)

type SpotRepository interface {
	GetSpots(longitude float64, latitude float64, radius float64, isCircle bool) ([]model.Spot, error)
}

type spotRepository struct {
	db *sql.DB
}

func NewSpotRepository(db *sql.DB) SpotRepository {
	return &spotRepository{db: db}
}

func (s spotRepository) GetSpots(longitude float64, latitude float64, radius float64, isCircle bool) ([]model.Spot, error) {
	var query string
	var spots []model.Spot
	if radius <= 50 {
		if isCircle {
			query = `SELECT *FROM my_table
        				WHERE ST_DWithin(my_table.geom,
        				                 ST_Buffer(ST_MakePoint?,?),?), 1) ORDER rating DESC`
		} else {
			query = `SELECT *FROM my_table
        				WHERE ST_DWithin(my_table.geom,
        				                 ST_Buffer(ST_MakePoint?,?),?), 0) ORDER rating DESC`
		}
	}
	if isCircle {
		query = `SELECT *FROM my_table
        				WHERE ST_DWithin(my_table.geom,
        				                 ST_Buffer(ST_MakePoint?,?),?), 1)`
	} else {
		query = `SELECT *FROM my_table
        				WHERE ST_DWithin(my_table.geom,
        				                 ST_Buffer(ST_MakePoint?,?),?), 0)`
	}

	rows, err := s.db.Query(query, longitude, latitude, radius)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var spot model.Spot
		err = rows.Scan(&spot.ID, &spot.Name, &spot.Website, &spot.Coordinates, &spot.Description, &spot.Rating)
		if err != nil {
			return nil, err
		}
		spots = append(spots, spot)
	}
	return spots, err
}
