package controller

import (
	"encoding/json"
	"net/http"
	"strconv"

	"spotlas/internal/repository"
)

type SpotController interface {
	GetSpots(w http.ResponseWriter, r *http.Request)
}

type spotController struct {
	repository repository.SpotRepository
}

func NewSpotController(repository repository.SpotRepository) SpotController {
	return &spotController{repository: repository}
}
func (s spotController) GetSpots(w http.ResponseWriter, r *http.Request) {
	lat := r.URL.Query().Get("latitude")
	long := r.URL.Query().Get("longitude")
	rad := r.URL.Query().Get("radius")
	isCircle := r.URL.Query().Has("second")

	latitude, err := strconv.ParseFloat(lat, 8)
	longitude, err := strconv.ParseFloat(long, 8)
	radius, err := strconv.ParseFloat(rad, 8)

	objects, err := s.repository.GetSpots(longitude, latitude, radius, isCircle)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(objects)
}
