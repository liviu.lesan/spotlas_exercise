package model

type Spot struct {
	ID          string
	Name        string
	Website     string
	Coordinates string
	Description string
	Rating      float64
}
