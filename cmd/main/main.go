package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"spotlas/internal/controller"
	"spotlas/internal/repository"
	"spotlas/utils/database"
)

var (
	db      = database.InitDB()
	repo    = repository.NewSpotRepository(db)
	handler = controller.NewSpotController(repo)
)

func main() {
	http.HandleFunc("/spots", handler.GetSpots)

	err := http.ListenAndServe(":8080", nil)

	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error starting server: %s\n", err)
		os.Exit(1)
	}
}
